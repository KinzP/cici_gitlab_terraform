# --- root/main.tf

module "vpc" {
  source       = "./vpc"
  vpc_cidr     = "10.0.0.0/16"
  public_cidrs = ["10.0.1.0/24", "10.0.2.0/24"]
}

module "ec2" {
  source        = "./ec2"
  web_sg        = var.vpc.web_sg
  public_subnet = module.vpc.public_subnet
}